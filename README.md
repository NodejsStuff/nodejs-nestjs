# nodejs-nestjs

Instalar [Nestjs](https://nestjs.com/) según su documentación oficial.

**1)** Instalar **el cli de Nest**

```
npm i -g @nestjs/cli   
```

**2)** Crear proyecto nest

```
nest new project-name
```

**3)** Se usa [TypeORM](https://typeorm.io/) como orm (documentación desde [Nestjs](https://docs.nestjs.com/techniques/database) para typeorm)

Ejemplo para mayslq2:
```
$ npm install --save @nestjs/typeorm typeorm mysql2
```

Comandos útiles:

Generar desde el cli un controller
```
nest g co mensajes
```
Generar una clase
```
nest g cl mensajes/dto/createMensajesDto
```
