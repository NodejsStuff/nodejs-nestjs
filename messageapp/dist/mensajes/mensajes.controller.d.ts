import { CreateMensajesDto } from './dto/create-mensajes-dto';
export declare class MensajesController {
    create(createMensajesDto: CreateMensajesDto): string;
    getAll(): string;
    update(updateMesajeDto: CreateMensajesDto): string;
}
